# Command line instructions
You can also upload existing files from your computer using the instructions below.

```
Git global setup
git config --global user.name "killy |0veufOrever"
git config --global user.email "80536642@qq.com"
```

# Create a new repository
```
git clone https://invent.kde.org/killy/hello.git
cd hello
git switch -c master
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

# Push an existing folder
```
cd existing_folder
git init --initial-branch=master
git remote add origin https://invent.kde.org/killy/hello.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

# Push an existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://invent.kde.org/killy/hello.git
git push -u origin --all
git push -u origin --tags
```
